#!/usr/bin/env python

import xml.etree.cElementTree as eT
import dateutil.parser
import sys

"""
    Provides a way to calculate expenses that contains IVA 10% under 
    Paraguayan laws, using the XML exported from GnuCash Android App
    and a special note in every transaction that contains IVA10,
    the tag in this implementation is `I10` precisely 
"""

__author__ = 'Diego Ramírez'

"""
    CONSTANTS
"""
SLOTS_TAG = "{http://www.gnucash.org/XML/trn}slots"
DATE_ENTERED_TAG = "{http://www.gnucash.org/XML/trn}date-entered"
SPLITS_TAG = "{http://www.gnucash.org/XML/trn}splits"
DESC_TAG = "{http://www.gnucash.org/XML/trn}description"
VALUE_TAG = "{http://www.gnucash.org/XML/slot}value"
KEY_TAG = ""  # FUTURE
QUANTITY_TAG = "{http://www.gnucash.org/XML/split}quantity"
TRANSACTION_TAG = "{http://www.gnucash.org/XML/gnc}transaction"


def parse_root(xmlstr=None):
    # GNC-V2 root
    tree = eT.fromstring(xmlstr)

    # gnc:book
    book_element = tree.getchildren()[1]

    # gnc:transaction
    trans_iter = list(book_element.iter(TRANSACTION_TAG))
    for i in range(1, 13):
        suma = 0.0
        for t in trans_iter:
            t_childrens = t.getchildren()
            # A given transaction could be counted ?
            if is_countable(t_childrens) and get_date(t_childrens).month == i:
                suma = suma + count_t(t_childrens)
        print("For the month " + str(i) + " the total of IVA10 included products is " + str(suma))


# given the children of a transaction determines if this one is countable
# TODO: support IVA 5 and IVA 10 together
def is_countable(t_childrens):
    for child in filter(lambda c: c.tag == SLOTS_TAG, t_childrens):
        slots = child.getchildren()
        for slot in slots:
            for s_element in slot.getchildren():
                if s_element.tag == VALUE_TAG and "i10" in s_element.text.lower():
                    return True
    return False


def get_date(t_childrens):
    for child in t_childrens:
        if DATE_ENTERED_TAG == child.tag:
            date_str_node = child.getchildren()[0]
            date = dateutil.parser.parse(date_str_node.text)
            return date


def count_t(t_childrens):
    for child in t_childrens:
        # if child.tag == DESC_TAG:
        #    print(child.text)
        splits = child.iter(SPLITS_TAG)
        for s in splits:
            split = s.getchildren()[0]
            q_element = split.iter(QUANTITY_TAG)
            for q in q_element:
                return abs(eval(q.text))


"""
    Script entry point function: Handle correctly the parameters received and call the parse_root function to start 
    the xml parsing.
"""
if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('No file_name is received as parameter')
        print('usage ./parse_gnca.py file_name')
        exit(-1)

    file_name = sys.argv[1]
    print("Trying to read %s" % file_name)
    with open(file_name) as f:
        xmlstr = f.read()
    parse_root(xmlstr=xmlstr)
    print("GNCA parser finished")
